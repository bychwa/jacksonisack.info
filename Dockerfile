FROM nginx:1.12-alpine
WORKDIR /app
ENV PORT_EXTERNAL 80
EXPOSE $PORT_EXTERNAL
COPY ./src/ ./
RUN chmod  644 ./*
COPY nginx.conf /etc/nginx/conf.d/default.conf